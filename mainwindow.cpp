#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

// слоты относящиеся к схеме Блома.

void MainWindow::on_DS_init_matrix_clicked() // инициализация матрицы
{
    if((ui->DS_matrix_size->text().replace(" ","")=="")||(ui->DS_GR_size->text().replace(" ","")=="")){ // проверка условий для инициализации
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Задайте кол-во абонентов и модуль поля.")); // вывод ошибки
    }else{
        // общая структура подготовки виджета таблицы
        // сначала очищаем таблицу
        // затем создаем в таблице нужное количество строк и столбцов
        while(ui->DS_matrix->rowCount()!=0)// очищаем в матрице строки
        {
            ui->DS_matrix->removeRow(0);
        }
        while(ui->DS_matrix->columnCount()!=0)// очищаем в матрице столбцы
        {
            ui->DS_matrix->removeColumn(0);
        }

        for ( int i = 0; i < ui->DS_matrix_size->text().toInt(); i++ )// создаем в матрице пустые строки
        {
            ui->DS_matrix->insertRow(i);
        }
        for ( int i = 0; i < ui->DS_matrix_size->text().toInt(); i++ )// создаем в матрице пустые столбцы
        {
            ui->DS_matrix->insertColumn(i);
        }

        for (int i = 0; i < ui->DS_matrix_size->text().toInt(); i++) {// заполняем матрицу
            for (int j = 0; j < ui->DS_matrix_size->text().toInt(); j++) {
                if (j >= i) {
                    int rand_val = rand() % ui->DS_GR_size->text().toInt(); // получаем случайное значение
                    ui->DS_matrix->setItem(i,j, new QTableWidgetItem(QString::number(rand_val))); // пишем его в ячейку матрицы
                    ui->DS_matrix->setItem(j,i, new QTableWidgetItem(QString::number(rand_val))); // и пишем в ячейку сметричную предыдущей по диагонали
                }
            }
        }
        ui->DS_matrix_state->setText("Матрица: инициализирована"); // меняем статус
    }
}

void MainWindow::on_A_create_my_public_key_clicked() // создать итендификатор/открытый ключ
{
    if(ui->DS_matrix_state->text()=="Матрица: не инициализирована"){ // проверяем условия, нам нужен размер матрицы и модуль
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Алиса не может узнать какой размерности матрица, поэтому не может создать итендификатор."));
    }else{
        // аналогичная подноготовка виджета таблицы
        while(ui->A_my_public_key->rowCount()!=0)// очищаем строки
        {
            ui->A_my_public_key->removeRow(0);
        }
        while(ui->A_my_public_key->columnCount()!=0)// очищаем столбцы
        {
            ui->A_my_public_key->removeColumn(0);
        }

        ui->A_my_public_key->insertRow(0);// добавляем строку
        for ( int i = 0; i < ui->DS_matrix->columnCount(); i++ )// создаем пустые столбцы
        {
            ui->A_my_public_key->insertColumn(i);
        }

        for (int i = 0; i < ui->DS_matrix->columnCount(); i++) {//заполняем итендификатор Алисы случайными значениями по модулю, который использовался при инициализации матрицы
                int rand_val = rand() % ui->DS_GR_size->text().toInt();
                ui->A_my_public_key->setItem(0,i, new QTableWidgetItem(QString::number(rand_val)));
        }
        ui->A_my_public_key_state->setText("Мой итендификатор: создан"); // меняем статус
    }
}

void MainWindow::on_A_to_send_DS_my_public_key_clicked() // отправка итендификатора/открытого ключа доверенной стороне
{
    if(ui->A_my_public_key_state->text()=="Мой итендификатор: не создан"){ // проверка и вывод ошибки, а создан ли вообще итендификатор
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Алиса еще не создала итендификатор."));
    }else{
        //подготовка виджета таблицы
        while(ui->DS_Alise_public_key->rowCount()!=0)// очищаем строки
        {
            ui->DS_Alise_public_key->removeRow(0);
        }
        while(ui->DS_Alise_public_key->columnCount()!=0)// очищаем столбцы
        {
            ui->DS_Alise_public_key->removeColumn(0);
        }
        ui->DS_Alise_public_key->insertRow(0);// строку пустые столбцы
        for ( int i = 0; i < ui->A_my_public_key->columnCount(); i++ )// создаем столбцы и заносим в них значения итендификатора/открытого ключа Алисы
        {
            ui->DS_Alise_public_key->insertColumn(i);
            ui->DS_Alise_public_key->setItem(0,i, new QTableWidgetItem(ui->A_my_public_key->item(0,i)->text()));
        }
        ui->DS_Alise_public_key_state->setText("Итендификатор Алисы: получен"); // меняем статусы
        ui->A_my_public_key_state_2->setText(",ДС отправлен");
    }
}

void MainWindow::on_A_to_send_Bob_my_public_key_clicked() // отправить итендификатор/открытый ключ Бобу
{
    // аналогичная процедура предыдущей, только взаимодействие с Бобом, а не доверенной стороной
    if(ui->A_my_public_key_state->text()=="Мой итендификатор: не создан"){
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Алиса еще не создала итендификатор."));
    }else{
        while(ui->B_Alise_public_key->rowCount()!=0)
        {
            ui->B_Alise_public_key->removeRow(0);
        }
        while(ui->B_Alise_public_key->columnCount()!=0)
        {
            ui->B_Alise_public_key->removeColumn(0);
        }
        ui->B_Alise_public_key->insertRow(0);
        for ( int i = 0; i < ui->A_my_public_key->columnCount(); i++ )
        {
            ui->B_Alise_public_key->insertColumn(i);
            ui->B_Alise_public_key->setItem(0,i, new QTableWidgetItem(ui->A_my_public_key->item(0,i)->text()));
        }
        ui->B_Alise_public_key_state->setText("Итендификатор Алисы: получен");
        ui->A_my_public_key_state_3->setText(",Бобу отправлен");
    }
}

void MainWindow::on_DS_calculate_Alise_private_key_clicked()// вычислить Алисе закрытый ключ
{
    if(ui->DS_Alise_public_key_state->text()=="Итендификатор Алисы: не получен"){ // проверяем можем ли мы его вообще вычислить
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Алиса еще не прислала свой итендификатор."));
    }else{
        // подготавливаем виджет таблицы
        while(ui->DS_Alise_private_key->rowCount()!=0)// очищаем строки
        {
            ui->DS_Alise_private_key->removeRow(0);
        }
        while(ui->DS_Alise_private_key->columnCount()!=0)// очищаем столбцы
        {
            ui->DS_Alise_private_key->removeColumn(0);
        }
        ui->DS_Alise_private_key->insertRow(0);// добавляем строку
        for ( int i = 0; i < ui->DS_matrix->columnCount(); i++ )// создаем столбцы
        {
            ui->DS_Alise_private_key->insertColumn(i); // добавляем столбец
            int buff_val=0; // буфер для умножения
            for(int j = 0 ; j < ui->DS_Alise_public_key->columnCount(); j++){ // умножаем матрицу на вектор олицетворяющий итендификатор/открытый ключ
                buff_val += (ui->DS_matrix->item(i, j)->text().toInt()*ui->DS_Alise_public_key->item(0, j)->text().toInt()); // непосредственно умножение и суммирование
                buff_val = buff_val%(ui->DS_GR_size->text().toInt()); // взятие модуля у результата
            }
            ui->DS_Alise_private_key->setItem(0,i, new QTableWidgetItem(QString::number(buff_val))); // записываем полученное значение
        }
        ui->DS_Alise_private_key_state->setText("Закрытый ключ Алисы: создан"); // меняем статус
    }
}

void MainWindow::on_DS_to_send_Alise_her_private_key_clicked() // отправляем полученный закрытый ключ Алисе
{
    if(ui->DS_Alise_private_key_state->text()=="Закрытый ключ Алисы: не создан"){ // проверяем вычисляли ли мы его
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Необходимо вычислить закрытый ключ Алисы, что бы его отправить."));
    }else{
        // подготавливаем виджет таблицы
        while(ui->A_my_private_key->rowCount()!=0)
        {
            ui->A_my_private_key->removeRow(0);
        }
        while(ui->A_my_private_key->columnCount()!=0)
        {
            ui->A_my_private_key->removeColumn(0);
        }
        ui->A_my_private_key->insertRow(0);
        for ( int i = 0; i < ui->DS_Alise_private_key->columnCount(); i++ )
        {
            ui->A_my_private_key->insertColumn(i);
            ui->A_my_private_key->setItem(0,i, new QTableWidgetItem(ui->DS_Alise_private_key->item(0,i)->text())); // копируем значения закрытого ключа
        }
        ui->DS_Alise_private_key_state_2->setText("Закрытый ключ Алисы: отправлен"); // меняем статусы
        ui->A_my_private_key_state->setText("Мой закрытый ключ от ДС: получен");
    }
}

void MainWindow::on_A_calculate_session_key_clicked() // вычисление сессионнго ключа
{
    if((ui->A_my_private_key_state->text()=="Мой закрытый ключ от ДС: не получен")||(ui->A_Bob_public_key_state->text()=="Итендификатор от Боба: не получен")){ // проверяем хватает ли нам данных
        // для вычисления на стороне Алисы сессионнго ключа должен быть известен ее закрытый ключ и открытый ключ Боба
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("У Алисы не хватает данных для расчета сеансового ключа для обмена сообщениями с Бобом."));
    }else{
        int buff_val=0; // буфферное значение
        for(int i = 0 ; i < ui->A_my_private_key->columnCount(); i++){ // перемножение двух векторов
            buff_val += (ui->A_my_private_key->item(0, i)->text().toInt()*ui->A_Bob_public_key->item(0, i)->text().toInt()); // умножение и суммирование эелементов
            buff_val = buff_val%(ui->DS_GR_size->text().toInt()); // взятие по модулю
        }
        ui->A_key_out->setText(QString::number(buff_val)); // записываем полученное значение на ключ для отправки сообщений
        ui->A_key_in->setText(QString::number(buff_val)); // записываем полученное значение на ключ для приема сообщений
        ui->A_session_key_state->setText("Сессионный ключ для обмена с Бобом: рассчитан"); // меняем статус
    }
}

// далее идут слоты аналогичные вышеописанным, за исключением того что они относятся к Бобу

void MainWindow::on_B_create_my_public_key_clicked() // создать итендификатор открытый ключ
{
    if(ui->DS_matrix_state->text()=="Матрица: не инициализирована"){
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Боб не может узнать какой размерности матрица, поэтому не может создать итендификатор."));
    }else{
        while(ui->B_my_public_key->rowCount()!=0)
        {
            ui->B_my_public_key->removeRow(0);
        }
        while(ui->B_my_public_key->columnCount()!=0)
        {
            ui->B_my_public_key->removeColumn(0);
        }

        ui->B_my_public_key->insertRow(0);
        for ( int i = 0; i < ui->DS_matrix->columnCount(); i++ )
        {
            ui->B_my_public_key->insertColumn(i);
        }

        for (int i = 0; i < ui->DS_matrix->columnCount(); i++) {
                int rand_val = rand() % ui->DS_GR_size->text().toInt();
                ui->B_my_public_key->setItem(0,i, new QTableWidgetItem(QString::number(rand_val)));
        }
        ui->B_my_public_key_state->setText("Мой итендификатор: создан");
    }
}

void MainWindow::on_B_to_send_DS_my_public_key_clicked() // отправить итендификатор/открытый ключ доверенной стороне
{
    if(ui->B_my_public_key_state->text()=="Мой итендификатор: не создан"){
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Боб еще не создал итендификатор."));
    }else{
        while(ui->DS_Bob_public_key->rowCount()!=0)
        {
            ui->DS_Bob_public_key->removeRow(0);
        }
        while(ui->DS_Bob_public_key->columnCount()!=0)
        {
            ui->DS_Bob_public_key->removeColumn(0);
        }
        ui->DS_Bob_public_key->insertRow(0);
        for ( int i = 0; i < ui->B_my_public_key->columnCount(); i++ )
        {
            ui->DS_Bob_public_key->insertColumn(i);
            ui->DS_Bob_public_key->setItem(0,i, new QTableWidgetItem(ui->B_my_public_key->item(0,i)->text()));
        }
        ui->DS_Bob_public_key_state->setText("Итендификатор Боба: получен");
        ui->B_my_public_key_state_2->setText(",ДС отправлен");
    }
}

void MainWindow::on_B_to_send_Alise_my_public_key_clicked() // отправить итендификатор/открытый ключ Алисе
{
    if(ui->B_my_public_key_state->text()=="Мой итендификатор: не создан"){
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Боб еще не создал итендификатор."));
    }else{
        while(ui->A_Bob_public_key->rowCount()!=0)
        {
            ui->A_Bob_public_key->removeRow(0);
        }
        while(ui->A_Bob_public_key->columnCount()!=0)
        {
            ui->A_Bob_public_key->removeColumn(0);
        }
        ui->A_Bob_public_key->insertRow(0);// строку пустые столбцы
        for ( int i = 0; i < ui->B_my_public_key->columnCount(); i++ )
        {
            ui->A_Bob_public_key->insertColumn(i);
            ui->A_Bob_public_key->setItem(0,i, new QTableWidgetItem(ui->B_my_public_key->item(0,i)->text()));
        }
        ui->A_Bob_public_key_state->setText("Итендификатор Боба: получен");
        ui->B_my_public_key_state_3->setText(",Алисе отправлен");
    }
}

void MainWindow::on_DS_calculate_Bob_private_key_clicked() // вычислить Бобу закрытый ключ
{
    if(ui->DS_Bob_public_key_state->text()=="Итендификатор Боба: не получен"){
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Боб еще не прислал свой итендификатор."));
    }else{
        while(ui->DS_Bob_private_key->rowCount()!=0)
        {
            ui->DS_Bob_private_key->removeRow(0);
        }
        while(ui->DS_Bob_private_key->columnCount()!=0)
        {
            ui->DS_Bob_private_key->removeColumn(0);
        }
        ui->DS_Bob_private_key->insertRow(0);
        for ( int i = 0; i < ui->DS_matrix->columnCount(); i++ )
        {
            ui->DS_Bob_private_key->insertColumn(i);
            int buff_val=0;
            for(int j = 0 ; j < ui->DS_Bob_public_key->columnCount(); j++){
                buff_val += (ui->DS_matrix->item(i, j)->text().toInt()*ui->DS_Bob_public_key->item(0, j)->text().toInt());
                buff_val = buff_val%(ui->DS_GR_size->text().toInt());
            }
            ui->DS_Bob_private_key->setItem(0,i, new QTableWidgetItem(QString::number(buff_val)));
        }
        ui->DS_Bob_private_key_state->setText("Закрытый ключ Боба: создан");
    }
}

void MainWindow::on_DS_to_send_Bob_him_private_key_clicked() // отправить Бобу закрытый ключ
{
    if(ui->DS_Bob_private_key_state->text()=="Закрытый ключ Боба: не создан"){
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Необходимо вычислить закрытый ключ Боба, что бы его отправить."));
    }else{
        while(ui->B_my_private_key->rowCount()!=0)
        {
            ui->B_my_private_key->removeRow(0);
        }
        while(ui->B_my_private_key->columnCount()!=0)
        {
            ui->B_my_private_key->removeColumn(0);
        }
        ui->B_my_private_key->insertRow(0);// строку пустые столбцы
        for ( int i = 0; i < ui->DS_Bob_private_key->columnCount(); i++ )
        {
        ui->B_my_private_key->insertColumn(i);
            ui->B_my_private_key->setItem(0,i, new QTableWidgetItem(ui->DS_Bob_private_key->item(0,i)->text()));
        }
        ui->DS_Bob_private_key_state_2->setText("Закрытый ключ Боба: отправлен");
        ui->B_my_private_key_state->setText("Мой закрытый ключ от ДС: получен");
    }
}

void MainWindow::on_B_calculate_session_key_clicked() // вычислить сессионный ключ
{
    if((ui->B_my_private_key_state->text()=="Мой закрытый ключ от ДС: не получен")||(ui->B_Alise_public_key_state->text()=="Итендификатор от Алисы: не получен")){
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("У Боба не хватает данных для расчета сеансового ключа для обмена сообщениями с Алисой."));
    }else{
        int buff_val=0;
        for(int i = 0 ; i < ui->B_my_private_key->columnCount(); i++){
            buff_val += (ui->B_my_private_key->item(0, i)->text().toInt()*ui->B_Alise_public_key->item(0, i)->text().toInt());
            buff_val = buff_val%(ui->DS_GR_size->text().toInt());
        }
        ui->B_key_out->setText(QString::number(buff_val));
        ui->B_key_in->setText(QString::number(buff_val));
        ui->B_session_key_state->setText("Сессионный ключ для обмена с Алисой: рассчитан");
    }
}

// слоты относящиеся к RC4 и отправке сообщений

void MainWindow::on_A_init_key_out_clicked() // провести инициализацию ключа для отправки сообщений на стороне Алисы
{
    if(ui->A_key_out->text().replace(" ","")==""){ // проверяем поле с ключем
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Алиса не может инициализировать ключ, он не указан.")); // если оно путсое выдаем ошибку
    }else{
        A_rc4_out.init(ui->A_key_out->text().replace(" ","").toUtf8()); // вызываем функцию инициализации
        ui->A_key_out_state->setText("Ключ: " + ui->A_key_out->text().replace(" ","")); // меняем статус
    }
}

// остальные слоты для инициализации ключей аналогичны

void MainWindow::on_A_init_key_in_clicked() // провести инициализацию ключа для приема сообщений на стороне Алисы
{
    if(ui->A_key_in->text().replace(" ","")==""){
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Алиса не может инициализировать ключ, он не указан."));
    }else{
        A_rc4_in.init(ui->A_key_in->text().replace(" ","").toUtf8());
        ui->A_key_in_state->setText("Ключ: " + ui->A_key_in->text().replace(" ",""));
    }
}

void MainWindow::on_B_init_key_out_clicked() // провести инициализацию ключа для отправки сообщений на стороне Боба
{
    if(ui->B_key_out->text().replace(" ","")==""){
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Боб не может инициализировать ключ, он не указан."));
    }else{
        B_rc4_out.init(ui->B_key_out->text().replace(" ","").toUtf8());
        ui->B_key_out_state->setText("Ключ: " + ui->B_key_out->text().replace(" ",""));
    }
}

void MainWindow::on_B_init_key_in_clicked() // провести инициализацию ключа для приема сообщений на стороне Боба
{
    if(ui->B_key_in->text().replace(" ","")==""){
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Боб не может инициализировать ключ, он не указан."));
    }else{
        B_rc4_in.init(ui->B_key_in->text().replace(" ","").toUtf8());
        ui->B_key_in_state->setText("Ключ: " + ui->B_key_in->text().replace(" ",""));
    }
}

void MainWindow::on_A_send_msg_Bob_clicked() // отправить Бобу сообщение
{
    if(ui->A_key_out_state->text()=="Ключ: не инициализирован"){ // проверяем инициализирован ли ключ
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Алиса не готова отправлять сообщение, у нее не инициализирован ключ."));
    }else{
        if(ui->B_key_in_state->text()=="Ключ: не инициализирован"){ // проверяем инициализирован ли ключ
            QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Боб не готов принимать сообщения сообщение, у него не инициализирован ключ. На практике сообщение бы к нему пришло, но он не смог бы его расшифровать пока не инициализировал ключ. В нашем случае инициализация ключа на стороне Боба необходима для стабильной работы программы."));
        }else{
            QByteArray Ciphertext = A_rc4_out.code(ui->A_msg_for_Bob->toPlainText().toUtf8()); // вызываем функцию зашифровки, передаем в нее сообщение, результат помещаем в канал, у нас в его роли QByteArray
            ui->Ciphertext->setText(Ciphertext); // выводим результат в виджет, нужно для обучающего момента и написания отчета
            ui->B_msg_form_Alice->setText(B_rc4_in.code(Ciphertext)); // расшифровываем сообщение на стороне Боба и выводим результат в виджет
        }
    }
}

void MainWindow::on_B_send_msg_Alise_clicked() // отправить сообщение Алисе, аналогично предыдущему слоту
{
    if(ui->B_key_out_state->text()=="Ключ: не инициализирован"){
        QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Боб не готов отправлять сообщение, у него не инициализирован ключ."));
    }else{
        if(ui->A_key_in_state->text()=="Ключ: не инициализирован"){
            QMessageBox::critical(NULL,QObject::tr("Ошибка"),tr("Алиса не готова принимать сообщения сообщение, у нее не инициализирован ключ. На практике сообщение бы к ней пришло, но она не смогла бы его расшифровать пока не инициализировала ключ. В нашем случае инициализация ключа на стороне Алисы необходима для стабильной работы программы."));
        }else{
            QByteArray Ciphertext = B_rc4_out.code(ui->B_msg_for_Alice->toPlainText().toUtf8());
            ui->Ciphertext->setText(Ciphertext);
            ui->A_msg_form_Bob->setText(A_rc4_in.code(Ciphertext));
        }
    }
}
