#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>

#include "rc4.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    // 4 экземпляра класса отвечающих за все данные относящиеся к RC4,
    // а точнее к генератору гаммы.
    RC4 A_rc4_out; // на стороне Алисы для зашифровки сообщений
    RC4 A_rc4_in; // на тсороне Алисы для расшифровки сообщений
    RC4 B_rc4_out; // на стороне Боба для зашифровки сообщений
    RC4 B_rc4_in; // на стороне Боба для расшифровки сообщений

    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    // Все слоты имеют говорящее название.
    // Сторона вызывающая слот обозначается как:
    // A - Алиса,
    // DS - Доверенная сторона,
    // B - Боб.

    // слоты относящиеся к схеме Блома.

    void on_DS_init_matrix_clicked(); // инициализировать матрицу

    void on_A_create_my_public_key_clicked(); // создать итендификатор/открытый ключ

    void on_A_to_send_DS_my_public_key_clicked(); // отправить итендификатор/открытый ключ доверенной стороне

    void on_A_to_send_Bob_my_public_key_clicked(); // отправить итендификатор/открытый ключ Бобу

    void on_DS_calculate_Alise_private_key_clicked(); // вычислить Алисе закрытый ключ

    void on_DS_to_send_Alise_her_private_key_clicked(); // отправить Алисе закрытый ключ

    void on_A_calculate_session_key_clicked(); // вычислить сессионный ключ

    void on_B_create_my_public_key_clicked(); // создать итендификатор/открытый ключ

    void on_B_to_send_DS_my_public_key_clicked(); // отправить итендификатор/открытый ключ доверенной стороне

    void on_B_to_send_Alise_my_public_key_clicked(); // отправить итендификатор/открытый ключ Алисе

    void on_DS_calculate_Bob_private_key_clicked(); // вычислить Бобу закрытый ключ

    void on_DS_to_send_Bob_him_private_key_clicked(); // отправить Бобу закрытый ключ

    void on_B_calculate_session_key_clicked(); // вычислить сессионный ключ

    // слоты относящиеся к RC4

    void on_A_send_msg_Bob_clicked(); // отправить сообщение Бобу

    void on_B_send_msg_Alise_clicked(); // отправить сообщение Алисе

    void on_A_init_key_out_clicked(); // инициализировать ключ на шифрование

    void on_A_init_key_in_clicked(); // инициализировать ключ на расшифрование

    void on_B_init_key_out_clicked(); // инициализировать ключ на шифрование

    void on_B_init_key_in_clicked(); // инициализировать ключ на расшифрование

private:
    Ui::MainWindow *ui; // итерфейс
};

#endif // MAINWINDOW_H
