#ifndef RC4_H
#define RC4_H

#include <QWidget>
class RC4
{
public:
    int mod = 256; // количество эелементов S-блока, как правило 256.
    QByteArray code(QByteArray plaintext); // основная функция шифрует и расшифровывает
    void init(QByteArray key); // инициализировать ключ / задать условия для генератора гаммы / алгоитм KSA
private:
    QByteArray S; // S-блок
    int i = 0; // переменная для вспомогательный вычислений
    int j = 0; // переменная для вспомогательный вычислений
    QByteArray swap(QByteArray array, int ind1, int ind2); // поменять местами для элемента массива
    // QByteArray имеет метод swap, но он меняет для массива меж собой, а не эелементы в одном из них
    QByteArray kword(); // получить псевдослучайное слово из гаммы
};

#endif // RC4_H
